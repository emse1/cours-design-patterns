package com.company;

import com.company.util.UserOptionsPrompt;

public class Main {

    public static void main(String[] args) {
        UserOptionsPrompt userOptions = new UserOptionsPrompt();
        userOptions.prompt();

        ChatController chatController = new ChatController();

        if (userOptions.isChatSizeWarningActivated()) {
            CheckRemainingSpaceChatObserver checkRemainingSpaceChatObserver = new CheckRemainingSpaceChatObserver();
            chatController.addObserver(checkRemainingSpaceChatObserver);
        }

        switch(userOptions.getPreferredFormat()) {
            case HTML -> chatController.addObserver(new HtmlRenderChatObserver());
            case MARKDOWN -> chatController.addObserver(new MarkdownRenderChatObserver());
        }

        chatController.start();


    }
}

package com.company;

public class HtmlRenderChatObserver implements ChatControllerObserver {

    @Override
    public void update(AbstractChatControllerObservable chatController) {
        String res = "";
        for (ChatMessage message : chatController.getChatContent()) {
            res += String.format("<p><strong>%s</strong>: %s</p>\n", message.getAuthor(), message.getContent());
        }
        System.out.println(res);
    }

}
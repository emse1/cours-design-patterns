package com.company;

public class CheckRemainingSpaceChatObserver implements ChatControllerObserver {
    @Override
    public void update(AbstractChatControllerObservable chatController) {
        System.out.println(String.format("The chat has size for %s messages left", chatController.getCHAT_MAX_SIZE() - chatController.getChatContent().size()));
    }
}

package com.company;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractChatControllerObservable {
    List<ChatControllerObserver> observers = new ArrayList<>();

    public abstract List<ChatMessage> getChatContent();
    public abstract int getCHAT_MAX_SIZE();

    public void addObserver(ChatControllerObserver observer) {
        this.observers.add(observer);
    }

    public void removeObserver(ChatControllerObserver observer) {
        this.observers.remove(observer);
    }

    public void notifyObservers() {
        for (ChatControllerObserver obs : this.observers) {
            obs.update(this);
        }
    }


}

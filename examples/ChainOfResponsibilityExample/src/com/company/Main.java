package com.company;

import com.company.handlers.AbstractRequestHandler;
import com.company.handlers.CheckInStoreProductRequestHandler;
import com.company.handlers.LoggingProductRequestHandler;
import com.company.handlers.NegativeQuantityProductRequestHandler;
import com.company.model.ProductRequest;

public class Main
{
    public static void main(String[] args)
    {
        // Setup Chain of Responsibility
        AbstractRequestHandler hNegativeRequest = new NegativeQuantityProductRequestHandler();
        AbstractRequestHandler hInStore = new CheckInStoreProductRequestHandler();
        AbstractRequestHandler hLogger = new LoggingProductRequestHandler();

        hNegativeRequest.setSuccessor(hInStore);
        hInStore.setSuccessor(hLogger);


        // Send requests to the chain
        hNegativeRequest.handleRequest(new ProductRequest( "FORK", -1));
        System.out.println("=======================================");
        hNegativeRequest.handleRequest(new ProductRequest( "PLATE", 2));
        System.out.println("=======================================");
        hNegativeRequest.handleRequest(new ProductRequest( "KNIFE", 2));
    }
}
